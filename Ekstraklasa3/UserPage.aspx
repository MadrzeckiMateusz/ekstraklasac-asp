﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserPage.aspx.cs" Inherits="Ekstraklasa3.UserPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 79px;
        }
        .auto-style6 {
            width: 175px;
        }
        .auto-style7 {
            width: 203px;
        }
        .auto-style8 {
            width: 730px;
        }
        .auto-style9 {
            width: 641px;
        }
        .auto-style10 {
            width: 641px;
            height: 166px;
        }
        .auto-style11 {
            height: 166px;
        }
        .auto-style14 {
            width: 254px;
        }
        .auto-style15 {
            width: 497px;
        }
        .auto-style16 {
            height: 166px;
            width: 160px;
        }
        .auto-style17 {
            width: 160px;
        }
        .auto-style18 {
            width: 513px;
        }
        .auto-style19 {
            width: 151px;
        }
        .auto-style22 {
            width: 375px;
        }
        .auto-style23 {
            width: 52px;
        }
        .auto-style25 {
            width: 24px;
        }
    </style>
</head>
<body style="height: 477px; z-index: 1; left: 0px; top: 0px; position: absolute; width: 1345px">
    <form id="form1" runat="server">
    <div>
    
        <div style="height: 48px; width: 1345px; background-color: #008000;">
            <asp:Label ID="Label1" runat="server" style="z-index: 1; left: 21px; top: 7px; position: absolute; width: 181px; right: 1143px; color: #0000CC; font-size: xx-large" Text="Ekstraklasa"></asp:Label>
            <asp:Label ID="Label5" runat="server" style="z-index: 1; left: 1013px; top: 14px; position: absolute; width: 313px"></asp:Label>
        </div>
    
    </div>
        <div style="height: 48px">
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" style="z-index: 1; left: 219px; top: 60px; position: absolute; height: 19px">Wyniki</asp:LinkButton>
            <asp:LinkButton ID="LinkButton2" runat="server" style="z-index: 1; left: 270px; top: 60px; position: absolute; right: 1008px" OnClick="LinkButton2_Click" >Rankingi</asp:LinkButton>
            <asp:LinkButton ID="LinkButton3" runat="server" style="z-index: 1; left: 332px; top: 59px; position: absolute" OnClick="LinkButton3_Click" >Harmonogram</asp:LinkButton>
            <asp:LinkButton ID="LinkButton4" runat="server" style="z-index: 1; left: 430px; top: 60px; position: absolute" OnClick="LinkButton4_Click">Raporty</asp:LinkButton>
            <asp:LinkButton ID="LinkButton5" runat="server" style="z-index: 1; left: 493px; top: 60px; position: absolute" OnClick="LinkButton5_Click">Zarządzanie Bazą</asp:LinkButton>
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" style="z-index: 1; left: 1189px; top: 53px; position: absolute; width: 145px; height: 30px" Text="Wyloguj" />
        </div>
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                Wyniki Spotkań<br />
                <asp:DropDownList ID="DropDownListspotkania1" runat="server" DataSourceID="SqlDataSourcedrużynaName" DataTextField="Nazwa_klubu" DataValueField="Nazwa_klubu" style="z-index: 1; left: 785px; top: 252px; position: absolute; height: 18px; width: 92px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSourcedrużynaName" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT [Nazwa_klubu] FROM [DRUŻYNY]"></asp:SqlDataSource>
                <asp:Label ID="Label4" runat="server" style="z-index: 1; left: 760px; top: 213px; position: absolute; height: 19px" Text="Wyszukiwanie"></asp:Label>
                <asp:CheckBox ID="CheckBoxspotkania2" runat="server" style="z-index: 1; left: 902px; top: 287px; position: absolute; height: 22px; width: 20px;" Text=" " />
                <asp:CheckBox ID="CheckBoxspotkania1" runat="server" style="z-index: 1; left: 901px; top: 255px; position: absolute" Text=" " />
                <asp:Label ID="Label2" runat="server" style="z-index: 1; left: 689px; top: 253px; position: absolute; width: 92px" Text="Drużyna: "></asp:Label>
                <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourcespotkania" ForeColor="#333333" GridLines="None" Width="673px">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Gospodarz" HeaderText="Gospodarz" SortExpression="Gospodarz" />
                        <asp:BoundField DataField="Wynik gospodarz" HeaderText="Wynik gospodarz" SortExpression="Wynik gospodarz" />
                        <asp:BoundField DataField="Wynik gość" HeaderText="Wynik gość" SortExpression="Wynik gość" />
                        <asp:BoundField DataField="Gość" HeaderText="Gość" SortExpression="Gość" />
                        <asp:BoundField DataField="Data Spotkania" HeaderText="Data Spotkania" ReadOnly="True" SortExpression="Data Spotkania" />
                        <asp:BoundField DataField="Kolejka" HeaderText="Kolejka" SortExpression="Kolejka" />
                    </Columns>
                    <EditRowStyle BackColor="#7C6F57" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                <asp:Button ID="wyszukajSpotkania" runat="server" OnClick="wyszukajSpotkania_Click" style="z-index: 1; left: 810px; top: 324px; position: absolute; width: 113px; height: 26px;" Text="Wyszukaj" />
                <asp:Label ID="Label3" runat="server" style="z-index: 1; left: 689px; top: 290px; position: absolute; width: 98px; height: 17px" Text="Kolejka: "></asp:Label>
                <asp:SqlDataSource ID="SqlDataSourcespotkania" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT g1.Gospodarz as Gospodarz, g1.[Wynik gospodarz] , g2.[Wynik gość], g2.Gość as Gość , g1.Data_Spotkania as 'Data Spotkania', g1.Kolejka FROM gospodarz_view g1 
LEFT JOIN gość_view g2 
ON g1.mecze = g2.mecz 
order by g1.kolejka;"></asp:SqlDataSource>
                <asp:DropDownList ID="DropDownListspotkania2" runat="server" DataSourceID="SqlDataSourcekolejka" DataTextField="Kolejka" DataValueField="Kolejka" style="z-index: 1; left: 786px; top: 286px; position: absolute; width: 93px; height: 18px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSourcekolejka" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT DISTINCT Kolejka FROM gość_view"></asp:SqlDataSource>
                <br />
            </asp:View>
            <asp:View ID="View2" runat="server">
                <table style="width: 22%;">
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="Label6" runat="server" style="width: 64px; " Text="Rankingi:"></asp:Label>
                        </td>
                        <td class="auto-style7">
                            <asp:LinkButton ID="LinkButton6" runat="server" OnClick="LinkButton6_Click" style="height: 20px;">Drużyn</asp:LinkButton>
                        </td>
                        <td class="auto-style6">
                            <asp:LinkButton ID="LinkButton7" runat="server" style="height: 20px" OnClick="LinkButton7_Click">Piłkarzy</asp:LinkButton>
                        </td>
                        <td>

                            <asp:LinkButton ID="LinkButton8" runat="server" OnClick="LinkButton8_Click">Bramkarzy</asp:LinkButton>

                        </td>
                    </tr>
                         
                </table>
                <br />
                <br />
                <asp:MultiView ID="MultiView2" runat="server">
                    <asp:View ID="View3" runat="server">
                        Ranking Drużyn<br /> 
                        <div style="height: 234px">
                            <asp:SqlDataSource ID="SqlDataSourcerankingdrużyn" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="Select d.Nazwa_klubu as 'Nazwa klubu', d.Barwy as Barwy, d.Pozycja_ligowa as 'Pozycja ligowa', d.Punkty as Punkty, 
d.Wygrane_mecze + d.Przegrane_mecze + d.Remisowe_mecze as 'Rozegrane mecze', d.ilość_zdobytych_bramek as 'Bramki zdobyte', d.ilość_straconych_bramek as 'Bramki stracone' from Drużyny d
order by d.Pozycja_ligowa; "></asp:SqlDataSource>
                            <table style="width:100%;">
                                <tr>
                                    <td class="auto-style8">
                                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourcerankingdrużyn" ForeColor="#333333" GridLines="None">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="Nazwa klubu" HeaderText="Nazwa klubu" SortExpression="Nazwa klubu" />
                                                <asp:BoundField DataField="Barwy" HeaderText="Barwy" SortExpression="Barwy" />
                                                <asp:BoundField DataField="Pozycja ligowa" HeaderText="Pozycja ligowa" SortExpression="Pozycja ligowa" />
                                                <asp:BoundField DataField="Punkty" HeaderText="Punkty" SortExpression="Punkty" />
                                                <asp:BoundField DataField="Rozegrane mecze" HeaderText="Rozegrane mecze" ReadOnly="True" SortExpression="Rozegrane mecze" />
                                                <asp:BoundField DataField="Bramki zdobyte" HeaderText="Bramki zdobyte" SortExpression="Bramki zdobyte" />
                                                <asp:BoundField DataField="Bramki stracone" HeaderText="Bramki stracone" SortExpression="Bramki stracone" />
                                            </Columns>
                                            <EditRowStyle BackColor="#7C6F57" />
                                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#E3EAEB" />
                                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                                        </asp:GridView>
                                    </td>
                                    <td>
                                        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" style="width: 179px" Text="Średni wiek zawodników" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <asp:Label ID="Label7" runat="server" Text="Średni wiek zawodników drużyn" Visible="False"></asp:Label>
                        <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourceAvgAGE" ForeColor="#333333" GridLines="None" Visible="False">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Wiek Średni" HeaderText="Wiek Średni" ReadOnly="True" SortExpression="Wiek Średni" />
                                <asp:BoundField DataField="Nazwa klubu" HeaderText="Nazwa klubu" SortExpression="Nazwa klubu" />
                            </Columns>
                            <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSourceAvgAGE" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="select AVG( Year(GETDATE())- YEAR( p.Data_ur)) as 'Wiek Średni'  ,d.Nazwa_klubu as 'Nazwa klubu' from 
 Piłkarze p , DRUŻYNY d 
Where p.ID_drużyny = d.ID_drużyny 
group by d.Nazwa_klubu;"></asp:SqlDataSource>
                        <br />
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        Ranking Piłkarzy<table style="width:100%;">
                            <tr>
                                <td class="auto-style10">
                                    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourcePiłkarzeRanking" ForeColor="#333333" GridLines="None">
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                            <asp:BoundField DataField="Imię" HeaderText="Imię" SortExpression="Imię" />
                                            <asp:BoundField DataField="Data urodzenia" HeaderText="Data urodzenia" ReadOnly="True" SortExpression="Data urodzenia" />
                                            <asp:BoundField DataField="Pozycja" HeaderText="Pozycja" SortExpression="Pozycja" />
                                            <asp:BoundField DataField="Klub" HeaderText="Klub" SortExpression="Klub" />
                                            <asp:BoundField DataField="Bramki" HeaderText="Bramki" ReadOnly="True" SortExpression="Bramki" />
                                            <asp:BoundField DataField="Czerwone kartki" HeaderText="Czerwone kartki" ReadOnly="True" SortExpression="Czerwone kartki" />
                                            <asp:BoundField DataField="Żółte kartki" HeaderText="Żółte kartki" ReadOnly="True" SortExpression="Żółte kartki" />
                                        </Columns>
                                        <EditRowStyle BackColor="#7C6F57" />
                                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#E3EAEB" />
                                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                        <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                        <SortedAscendingHeaderStyle BackColor="#246B61" />
                                        <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                        <SortedDescendingHeaderStyle BackColor="#15524A" />
                                    </asp:GridView>
                                </td>
                                <td class="auto-style16">
                                    <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="Król strzelców" Width="154px" />
                                    <div style="height: 125px">
                                    </div>
                                </td>
                                <td class="auto-style11">
                                    <asp:Label ID="Label8" runat="server" Text="Król Strzelców" Visible="False"></asp:Label>
                                    <asp:SqlDataSource ID="SqlDataSourceKrolStrzelcow" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="select Nazwisko , Imię , Pozycja , klub , bramka as gole from strzelcy Where bramka like '2'
order by bramka desc ;"></asp:SqlDataSource>
                                    <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourceKrolStrzelcow" ForeColor="#333333" GridLines="None" Visible="False">
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                            <asp:BoundField DataField="Imię" HeaderText="Imię" SortExpression="Imię" />
                                            <asp:BoundField DataField="Pozycja" HeaderText="Pozycja" SortExpression="Pozycja" />
                                            <asp:BoundField DataField="klub" HeaderText="klub" SortExpression="klub" />
                                            <asp:BoundField DataField="gole" HeaderText="gole" SortExpression="gole" />
                                        </Columns>
                                        <EditRowStyle BackColor="#7C6F57" />
                                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#E3EAEB" />
                                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                        <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                        <SortedAscendingHeaderStyle BackColor="#246B61" />
                                        <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                        <SortedDescendingHeaderStyle BackColor="#15524A" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style9">
                                    <asp:SqlDataSource ID="SqlDataSourcePiłkarzeRanking" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="Select p.nazwisko as 'Nazwisko',p.imie as Imię, CONVERT(date,p.Data_ur,120) as 'Data urodzenia', 
p.Pozycja as 'Pozycja', d.Nazwa_klubu as Klub, sum(z.gol) as 'Bramki', sum(z.Czerwona_kartka) as 'Czerwone kartki', sum(z.Żółta_kartka) as 'Żółte kartki' 
from PIŁKARZE p 
left join ZDARZENIA z 
on p.ID_piłkarza=z.ID_piłkarza, DRUŻYNY d 
where p.ID_drużyny = d.ID_drużyny and p.pozycja&lt;&gt;'Bramkarz'
group by p.Nazwisko, p.Imie,p.Data_ur,p.Pozycja , d.Nazwa_klubu 
order by Bramki desc ;"></asp:SqlDataSource>
                                </td>
                                <td class="auto-style17">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style9">&nbsp;</td>
                                <td class="auto-style17">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <br />
                        </asp:View>
                    <asp:View ID="View5" runat="server">
                        <asp:Label ID="Label9" runat="server" Text="Ranking Bramkarzy"></asp:Label>
                        <br />
                        <asp:GridView ID="GridView6" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourceRankingbramkarzy" ForeColor="#333333" GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                <asp:BoundField DataField="Imię" HeaderText="Imię" SortExpression="Imię" />
                                <asp:BoundField DataField="Data urodzenia" HeaderText="Data urodzenia" ReadOnly="True" SortExpression="Data urodzenia" />
                                <asp:BoundField DataField="Pozycja" HeaderText="Pozycja" SortExpression="Pozycja" />
                                <asp:BoundField DataField="Klub" HeaderText="Klub" SortExpression="Klub" />
                                <asp:BoundField DataField="Stracone Bramki" HeaderText="Stracone Bramki" ReadOnly="True" SortExpression="Stracone Bramki" />
                                <asp:BoundField DataField="Czerwone kartki" HeaderText="Czerwone kartki" ReadOnly="True" SortExpression="Czerwone kartki" />
                                <asp:BoundField DataField="Żółte kartki" HeaderText="Żółte kartki" ReadOnly="True" SortExpression="Żółte kartki" />
                            </Columns>
                            <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSourceRankingbramkarzy" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="Select p.nazwisko as 'Nazwisko',p.imie as Imię, CONVERT(date,p.Data_ur,120) as 'Data urodzenia', 
p.Pozycja as 'Pozycja', d.Nazwa_klubu as Klub, sum(z.Strata) as 'Stracone Bramki', 
sum(z.Czerwona_kartka) as 'Czerwone kartki', sum(z.Żółta_kartka) as 'Żółte kartki' 
from PIŁKARZE p 
				 left join ZDARZENIA z 
				 on p.ID_piłkarza=z.ID_piłkarza, DRUŻYNY d 
				 where p.ID_drużyny = d.ID_drużyny and p.pozycja = 'Bramkarz'
				 group by p.Nazwisko, p.Imie,p.Data_ur,p.Pozycja , d.Nazwa_klubu 
				 order by 'Stracone Bramki' desc;"></asp:SqlDataSource>
                        </asp:View>
                </asp:MultiView>
                <br />
                </asp:View>
            <asp:View ID="View6" runat="server">

                <asp:Label ID="Label10" runat="server" Text="Harmonogram"></asp:Label>
                <br />
                <table style="width:100%;">
                    <tr>
                        <td class="auto-style15">
                            <asp:GridView ID="GridView7" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSourceHarmonogram" ForeColor="#333333" GridLines="None">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Gospodarz" HeaderText="Gospodarz" SortExpression="Gospodarz" />
                                    <asp:BoundField DataField="Gość" HeaderText="Gość" SortExpression="Gość" />
                                    <asp:BoundField DataField="Data Spotkania" HeaderText="Data Spotkania" ReadOnly="True" SortExpression="Data Spotkania" />
                                    <asp:BoundField DataField="Kolejka" HeaderText="Kolejka" SortExpression="Kolejka" />
                                </Columns>
                                <EditRowStyle BackColor="#7C6F57" />
                                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#E3EAEB" />
                                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                <SortedAscendingHeaderStyle BackColor="#246B61" />
                                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                <SortedDescendingHeaderStyle BackColor="#15524A" />
                            </asp:GridView>
                        </td>
                        <td class="auto-style14">
                            <asp:Label ID="Label11" runat="server" style="width: 65px" Text="Data od "></asp:Label>
                            <asp:CheckBox ID="CheckBoxHarmo" runat="server" Text=" " />
                            <asp:Calendar ID="Calendar2" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" style="height: 120px; width: 228px">
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                            </asp:Calendar>
                        </td>
                        <td>
                            <asp:Label ID="Label12" runat="server" Text="Data do "></asp:Label>
                            <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="74px" style="height: 171px; width: 217px" Width="217px">
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                            </asp:Calendar>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style15">
                            <asp:SqlDataSource ID="SqlDataSourceHarmonogram" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT g1.Gospodarz as Gospodarz, g2.Gość as Gość , g1.Data_Spotkania as 'Data Spotkania', g1.Kolejka 
					FROM gospodarz_view g1 
					LEFT JOIN gość_view g2 
					ON g1.mecze = g2.mecz 
					order by g1.kolejka;"></asp:SqlDataSource>
                        </td>
                        <td class="auto-style14">
                            <asp:Button ID="Button5" runat="server" OnClick="Button5_Click" style="width: 141px" Text="Wyszukaj" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style15">&nbsp;</td>
                        <td class="auto-style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br />

                </asp:View>
           <asp:View ID="View7" runat="server">

               <asp:Label ID="Label13" runat="server" Text="Raporty"></asp:Label>
               <table style="width:100%;">
                   <tr>
                       <td class="auto-style18">
                           <asp:GridView ID="GridView8" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_sedziego" DataSourceID="SqlDataSourceRaporty" ForeColor="#333333" GridLines="None">
                               <AlternatingRowStyle BackColor="White" />
                               <Columns>
                                   <asp:BoundField DataField="ID_sedziego" HeaderText="ID_sedziego" ReadOnly="True" SortExpression="ID_sedziego" />
                                   <asp:BoundField DataField="imie" HeaderText="imie" SortExpression="imie" />
                                   <asp:BoundField DataField="nazwisko" HeaderText="nazwisko" SortExpression="nazwisko" />
                                   <asp:BoundField DataField="data urodzenia" HeaderText="data urodzenia" ReadOnly="True" SortExpression="data urodzenia" />
                                   <asp:BoundField DataField="Ilość meczy" HeaderText="Ilość meczy" ReadOnly="True" SortExpression="Ilość meczy" />
                               </Columns>
                               <EditRowStyle BackColor="#7C6F57" />
                               <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                               <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                               <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                               <RowStyle BackColor="#E3EAEB" />
                               <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                               <SortedAscendingCellStyle BackColor="#F8FAFA" />
                               <SortedAscendingHeaderStyle BackColor="#246B61" />
                               <SortedDescendingCellStyle BackColor="#D4DFE1" />
                               <SortedDescendingHeaderStyle BackColor="#15524A" />
                           </asp:GridView>
                           <asp:SqlDataSource ID="SqlDataSourceRaporty" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="Select s.ID_sedziego ,s.imie, s.nazwisko, convert(date,s.data_ur,120) as 'data urodzenia', 
count(sm.id_meczu) as 'Ilość meczy' from SĘDZIOWIE s left join sędzia_meczu sm on s.ID_sedziego = sm.ID_sedziego 
Where s.ID_sedziego &lt;&gt; ' '
group by s.imie,s.nazwisko,s.ID_sedziego,s.Data_ur;"></asp:SqlDataSource>
                       </td>
                       <td class="auto-style19">
                           <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSourceszczegołysedzia" DataTextField="Nazwisko" DataValueField="ID_sedziego">
                           </asp:DropDownList>
                           <asp:SqlDataSource ID="SqlDataSourceszczegołysedzia" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT [ID_sedziego], [Nazwisko] FROM [SĘDZIOWIE]"></asp:SqlDataSource>
                           <asp:Button ID="Button6" runat="server" OnClick="Button6_Click" Text="Wyświetl szczegóły" />
                       </td>
                       <td>
                           <asp:Label ID="Label14" runat="server" Text="Szczegóły pracy sędziego" Visible="False"></asp:Label>
                           <asp:GridView ID="GridView9" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Numer Zdarzenia" DataSourceID="SqlDataSourceSzczegółypracy" ForeColor="#333333" GridLines="None" Visible="False">
                               <AlternatingRowStyle BackColor="White" />
                               <Columns>
                                   <asp:BoundField DataField="Numer Zdarzenia" HeaderText="Numer Zdarzenia" ReadOnly="True" SortExpression="Numer Zdarzenia" />
                                   <asp:BoundField DataField="Numer Spotkania" HeaderText="Numer Spotkania" SortExpression="Numer Spotkania" />
                                   <asp:BoundField DataField="Numer Piłkarza" HeaderText="Numer Piłkarza" SortExpression="Numer Piłkarza" />
                                   <asp:BoundField DataField="Czas_zdarzenia" HeaderText="Czas_zdarzenia" SortExpression="Czas_zdarzenia" />
                                   <asp:BoundField DataField="Gol" HeaderText="Gol" SortExpression="Gol" />
                                   <asp:BoundField DataField="Strata" HeaderText="Strata" SortExpression="Strata" />
                                   <asp:BoundField DataField="Czerwona kartka" HeaderText="Czerwona kartka" SortExpression="Czerwona kartka" />
                                   <asp:BoundField DataField="Żółta kartka" HeaderText="Żółta kartka" SortExpression="Żółta kartka" />
                               </Columns>
                               <EditRowStyle BackColor="#7C6F57" />
                               <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                               <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                               <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                               <RowStyle BackColor="#E3EAEB" />
                               <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                               <SortedAscendingCellStyle BackColor="#F8FAFA" />
                               <SortedAscendingHeaderStyle BackColor="#246B61" />
                               <SortedDescendingCellStyle BackColor="#D4DFE1" />
                               <SortedDescendingHeaderStyle BackColor="#15524A" />
                           </asp:GridView>
                           <asp:SqlDataSource ID="SqlDataSourceSzczegółypracy" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="Select z.ID_zdarzenia as'Numer Zdarzenia', z.ID_meczu as 'Numer Spotkania',
                 z.ID_piłkarza as 'Numer Piłkarza', z.Czas_zdarzenia, z.Gol , z.Strata,
                z.Czerwona_kartka as 'Czerwona kartka', z.Żółta_kartka as 'Żółta kartka'
                 from ZDARZENIA z ,mecze m , Sędzia_meczu sm
               where sm.ID_sedziego = '1' and sm.ID_meczu = z.ID_meczu
                group by z.ID_meczu,z.ID_zdarzenia,z.ID_piłkarza,z.Czas_zdarzenia,z.Gol,z.Strata,z.Czerwona_kartka,z.Żółta_kartka;"></asp:SqlDataSource>
                       </td>
                   </tr>
                   <tr>
                       <td class="auto-style18">&nbsp;</td>
                       <td class="auto-style19">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
                   <tr>
                       <td class="auto-style18">&nbsp;</td>
                       <td class="auto-style19">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
               </table>

            </asp:View>
             <asp:View ID="View8" runat="server">
                 <asp:Label ID="Label15" runat="server" Text="Zarzadzanie bazą danych"></asp:Label>
                 <table style="width:100%;">
                     <tr>
                         <td class="auto-style23">
                             <asp:Label ID="Label17" runat="server" Text="Drużyny"></asp:Label>
                         </td>
                         <td class="auto-style25">
                             <asp:Button ID="Button8" runat="server" Text="Podgląd" OnClick="Button8_Click" />
                         </td>
                         <td class="auto-style22">
                             <asp:Button ID="Button9" runat="server" style="margin-left: 0px" Text="Zarządzanie" OnClick="Button9_Click" />
                         </td>
                     </tr>
                     <tr>
                         <td class="auto-style23">Mecze</td>
                         <td class="auto-style25">
                             <asp:Button ID="Button10" runat="server" Text="Podgląd" OnClick="Button10_Click" />
                         </td>
                         <td class="auto-style22">
                             <asp:Button ID="Button11" runat="server" Text="Zarządzanie" OnClick="Button11_Click" />
                         </td>
                     </tr>
                     <tr>
                         <td class="auto-style23">Piłkarze</td>
                         <td class="auto-style25">
                             <asp:Button ID="Button12" runat="server" Text="Podgląd" OnClick="Button12_Click" />
                         </td>
                         <td class="auto-style22">
                             <asp:Button ID="Button13" runat="server" Text="Zarządzanie" OnClick="Button13_Click" />
                         </td>
                     </tr>
                     <tr>
                          <td class="auto-style23">Sędziowie</td>
                         <td class="auto-style25">
                             <asp:Button ID="Button14" runat="server" Text="Podgląd" OnClick="Button14_Click" />
                          </td>
                         <td class="auto-style22">
                             <asp:Button ID="Button15" runat="server" Text="Zarządzanie" OnClick="Button15_Click" />
                          </td>
                     </tr>
                      <tr>
                          <td class="auto-style23">Sędzia meczu</td>
                         <td class="auto-style25">
                             <asp:Button ID="Button16" runat="server" Text="Podgląd" OnClick="Button16_Click" />
                          </td>
                         <td class="auto-style22">
                             <asp:Button ID="Button17" runat="server" Text="Zarzadzanie" OnClick="Button17_Click" />
                          </td>
                     </tr>
                      <tr>
                          <td class="auto-style23">Trenerzy</td>
                         <td class="auto-style25">
                             <asp:Button ID="Button18" runat="server" Text="Podgląd" OnClick="Button18_Click" />
                          </td>
                         <td class="auto-style22">
                             <asp:Button ID="Button19" runat="server" Text="Zarządzanie" OnClick="Button19_Click" />
                          </td>
                     </tr>
                     <tr>
                          <td class="auto-style23">Uzytkownicy</td>
                         <td class="auto-style25">
                             <asp:Button ID="Button20" runat="server" Text="Podgląd" OnClick="Button20_Click" />
                          </td>
                         <td class="auto-style22">
                             <asp:Button ID="Button21" runat="server" Text="Zarządznie" OnClick="Button21_Click" />
                          </td>
                     </tr>
                     <tr>
                          <td class="auto-style23">Zdarzenia</td>
                         <td class="auto-style25">
                             <asp:Button ID="Button22" runat="server" Text="Podgląd" OnClick="Button22_Click" />
                          </td>
                         <td class="auto-style22">
                             <asp:Button ID="Button23" runat="server" Text="Zarządznie" OnClick="Button23_Click" />
                          </td>
                     </tr>
                 </table>
                 <br />
                 <asp:MultiView ID="MultiView3" runat="server">
                     <asp:View ID="View9" runat="server">
                         <br />
                         <br />
                         <br />
                         <asp:Label ID="Label16" runat="server" Text="Podgląd tabeli drużyny"></asp:Label>
                         <asp:GridView ID="GridView10" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_drużyny" DataSourceID="SqlDataSourcemenagmentDruzyny" ForeColor="#333333" GridLines="None">
                             <AlternatingRowStyle BackColor="White" />
                             <Columns>
                                 <asp:BoundField DataField="ID_drużyny" HeaderText="ID_drużyny" ReadOnly="True" SortExpression="ID_drużyny" />
                                 <asp:BoundField DataField="Nazwa_klubu" HeaderText="Nazwa_klubu" SortExpression="Nazwa_klubu" />
                                 <asp:BoundField DataField="Barwy" HeaderText="Barwy" SortExpression="Barwy" />
                                 <asp:BoundField DataField="Data_zal" HeaderText="Data_zal" SortExpression="Data_zal" />
                                 <asp:BoundField DataField="Adres" HeaderText="Adres" SortExpression="Adres" />
                                 <asp:BoundField DataField="Pozycja_ligowa" HeaderText="Pozycja_ligowa" SortExpression="Pozycja_ligowa" />
                                 <asp:BoundField DataField="Punkty" HeaderText="Punkty" SortExpression="Punkty" />
                                 <asp:BoundField DataField="ilość_zdobytych_bramek" HeaderText="ilość_zdobytych_bramek" SortExpression="ilość_zdobytych_bramek" />
                                 <asp:BoundField DataField="ilość_straconych_bramek" HeaderText="ilość_straconych_bramek" SortExpression="ilość_straconych_bramek" />
                                 <asp:BoundField DataField="Wygrane_mecze" HeaderText="Wygrane_mecze" SortExpression="Wygrane_mecze" />
                                 <asp:BoundField DataField="Przegrane_mecze" HeaderText="Przegrane_mecze" SortExpression="Przegrane_mecze" />
                                 <asp:BoundField DataField="Remisowe_mecze" HeaderText="Remisowe_mecze" SortExpression="Remisowe_mecze" />
                             </Columns>
                             <EditRowStyle BackColor="#7C6F57" />
                             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                             <RowStyle BackColor="#E3EAEB" />
                             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                             <SortedAscendingCellStyle BackColor="#F8FAFA" />
                             <SortedAscendingHeaderStyle BackColor="#246B61" />
                             <SortedDescendingCellStyle BackColor="#D4DFE1" />
                             <SortedDescendingHeaderStyle BackColor="#15524A" />
                         </asp:GridView>
                         <asp:SqlDataSource ID="SqlDataSourcemenagmentDruzyny" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [DRUŻYNY]">
                         </asp:SqlDataSource>
                         <asp:Button ID="Button7" runat="server" OnClick="Button7_Click" Text="Odśwież" />
                         <br />
                     </asp:View>
                     <asp:View ID="View11" runat="server">
                         Podgląd tabeli Mecze<br />
                         <asp:GridView ID="GridView11" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_meczu" DataSourceID="SqlDataSourcemecze" ForeColor="#333333" GridLines="None">
                             <AlternatingRowStyle BackColor="White" />
                             <Columns>
                                 <asp:BoundField DataField="ID_meczu" HeaderText="ID_meczu" ReadOnly="True" SortExpression="ID_meczu" />
                                 <asp:BoundField DataField="Kolejka" HeaderText="Kolejka" SortExpression="Kolejka" />
                                 <asp:BoundField DataField="Data_meczu" HeaderText="Data_meczu" SortExpression="Data_meczu" />
                                 <asp:BoundField DataField="Godzina" HeaderText="Godzina" SortExpression="Godzina" />
                             </Columns>
                             <EditRowStyle BackColor="#7C6F57" />
                             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                             <RowStyle BackColor="#E3EAEB" />
                             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                             <SortedAscendingCellStyle BackColor="#F8FAFA" />
                             <SortedAscendingHeaderStyle BackColor="#246B61" />
                             <SortedDescendingCellStyle BackColor="#D4DFE1" />
                             <SortedDescendingHeaderStyle BackColor="#15524A" />
                         </asp:GridView>
                         <asp:SqlDataSource ID="SqlDataSourcemecze" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT * FROM [MECZE]"></asp:SqlDataSource>
                         <asp:Button ID="Button24" runat="server" OnClick="Button24_Click" Text="Odśwież" />
                         </asp:View>
                     <asp:View ID="View12" runat="server">
                         Podgląd Tabeli Piłkarze<br />
                         <asp:GridView ID="GridView12" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_piłkarza" DataSourceID="SqlDataSourcepiłkarze" ForeColor="#333333" GridLines="None">
                             <AlternatingRowStyle BackColor="White" />
                             <Columns>
                                 <asp:BoundField DataField="ID_piłkarza" HeaderText="ID_piłkarza" ReadOnly="True" SortExpression="ID_piłkarza" />
                                 <asp:BoundField DataField="ID_drużyny" HeaderText="ID_drużyny" SortExpression="ID_drużyny" />
                                 <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                 <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                                 <asp:BoundField DataField="Data_ur" HeaderText="Data_ur" SortExpression="Data_ur" />
                                 <asp:BoundField DataField="Pozycja" HeaderText="Pozycja" SortExpression="Pozycja" />
                                 <asp:BoundField DataField="Waga" HeaderText="Waga" SortExpression="Waga" />
                                 <asp:BoundField DataField="Wzrost" HeaderText="Wzrost" SortExpression="Wzrost" />
                                 <asp:BoundField DataField="Szybkosc" HeaderText="Szybkosc" SortExpression="Szybkosc" />
                             </Columns>
                             <EditRowStyle BackColor="#7C6F57" />
                             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                             <RowStyle BackColor="#E3EAEB" />
                             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                             <SortedAscendingCellStyle BackColor="#F8FAFA" />
                             <SortedAscendingHeaderStyle BackColor="#246B61" />
                             <SortedDescendingCellStyle BackColor="#D4DFE1" />
                             <SortedDescendingHeaderStyle BackColor="#15524A" />
                         </asp:GridView>
                         <asp:SqlDataSource ID="SqlDataSourcepiłkarze" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT * FROM [PIŁKARZE]"></asp:SqlDataSource>
                         <asp:Button ID="Button25" runat="server" OnClick="Button25_Click" Text="Odśwież" />
                         </asp:View>
                      <asp:View ID="View13" runat="server">
                          Podgląd Tabeli Sędziowie<asp:GridView ID="GridView13" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_sedziego" DataSourceID="SqlDataSourceSedziowie" ForeColor="#333333" GridLines="None">
                              <AlternatingRowStyle BackColor="White" />
                              <Columns>
                                  <asp:BoundField DataField="ID_sedziego" HeaderText="ID_sedziego" ReadOnly="True" SortExpression="ID_sedziego" />
                                  <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                  <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                                  <asp:BoundField DataField="Data_ur" HeaderText="Data_ur" SortExpression="Data_ur" />
                                  <asp:BoundField DataField="Ilość_przep_meczy" HeaderText="Ilość_przep_meczy" SortExpression="Ilość_przep_meczy" />
                              </Columns>
                              <EditRowStyle BackColor="#7C6F57" />
                              <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                              <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                              <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                              <RowStyle BackColor="#E3EAEB" />
                              <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                              <SortedAscendingCellStyle BackColor="#F8FAFA" />
                              <SortedAscendingHeaderStyle BackColor="#246B61" />
                              <SortedDescendingCellStyle BackColor="#D4DFE1" />
                              <SortedDescendingHeaderStyle BackColor="#15524A" />
                          </asp:GridView>
                          <asp:SqlDataSource ID="SqlDataSourceSedziowie" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT * FROM [SĘDZIOWIE]"></asp:SqlDataSource>
                          <asp:Button ID="Button26" runat="server" OnClick="Button26_Click" Text="Odśwież" />
                          </asp:View>
                     <asp:View ID="View14" runat="server">
                         Podgląd Tabeli Sędzia meczu<asp:GridView ID="GridView14" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_meczu,ID_sedziego" DataSourceID="SqlDataSourcesedziameczu" ForeColor="#333333" GridLines="None">
                             <AlternatingRowStyle BackColor="White" />
                             <Columns>
                                 <asp:BoundField DataField="ID_meczu" HeaderText="ID_meczu" ReadOnly="True" SortExpression="ID_meczu" />
                                 <asp:BoundField DataField="ID_sedziego" HeaderText="ID_sedziego" ReadOnly="True" SortExpression="ID_sedziego" />
                                 <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                             </Columns>
                             <EditRowStyle BackColor="#7C6F57" />
                             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                             <RowStyle BackColor="#E3EAEB" />
                             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                             <SortedAscendingCellStyle BackColor="#F8FAFA" />
                             <SortedAscendingHeaderStyle BackColor="#246B61" />
                             <SortedDescendingCellStyle BackColor="#D4DFE1" />
                             <SortedDescendingHeaderStyle BackColor="#15524A" />
                         </asp:GridView>
                         <asp:SqlDataSource ID="SqlDataSourcesedziameczu" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT * FROM [Sędzia_meczu]"></asp:SqlDataSource>
                         <br />
                         <asp:Button ID="Button27" runat="server" OnClick="Button27_Click" Text="Odśwież" />
                         </asp:View>
                     <asp:View ID="View15" runat="server">
                         Podgląd tabeli Trenerzy<asp:GridView ID="GridView15" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_trenera" DataSourceID="SqlDataSourcetrenerzy" ForeColor="#333333" GridLines="None">
                             <AlternatingRowStyle BackColor="White" />
                             <Columns>
                                 <asp:BoundField DataField="ID_trenera" HeaderText="ID_trenera" ReadOnly="True" SortExpression="ID_trenera" />
                                 <asp:BoundField DataField="ID_drużyny" HeaderText="ID_drużyny" SortExpression="ID_drużyny" />
                                 <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                 <asp:BoundField DataField="Imię" HeaderText="Imię" SortExpression="Imię" />
                                 <asp:BoundField DataField="Data_ur" HeaderText="Data_ur" SortExpression="Data_ur" />
                                 <asp:BoundField DataField="Stopien_trenera" HeaderText="Stopien_trenera" SortExpression="Stopien_trenera" />
                             </Columns>
                             <EditRowStyle BackColor="#7C6F57" />
                             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                             <RowStyle BackColor="#E3EAEB" />
                             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                             <SortedAscendingCellStyle BackColor="#F8FAFA" />
                             <SortedAscendingHeaderStyle BackColor="#246B61" />
                             <SortedDescendingCellStyle BackColor="#D4DFE1" />
                             <SortedDescendingHeaderStyle BackColor="#15524A" />
                         </asp:GridView>
                         <asp:SqlDataSource ID="SqlDataSourcetrenerzy" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT * FROM [TRENERZY]"></asp:SqlDataSource>
                         <asp:Button ID="Button28" runat="server" OnClick="Button28_Click" Text="Odśwież" />
                         </asp:View>
                     <asp:View ID="View16" runat="server">
                         Podgląd Tabeli Użytkownicy<br />
                         <asp:GridView ID="GridView16" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Nazwa_użytkownika" DataSourceID="SqlDataSourceuser" ForeColor="#333333" GridLines="None">
                             <AlternatingRowStyle BackColor="White" />
                             <Columns>
                                 <asp:BoundField DataField="Nazwa_użytkownika" HeaderText="Nazwa_użytkownika" ReadOnly="True" SortExpression="Nazwa_użytkownika" />
                                 <asp:BoundField DataField="Haslo" HeaderText="Haslo" SortExpression="Haslo" />
                                 <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                             </Columns>
                             <EditRowStyle BackColor="#7C6F57" />
                             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                             <RowStyle BackColor="#E3EAEB" />
                             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                             <SortedAscendingCellStyle BackColor="#F8FAFA" />
                             <SortedAscendingHeaderStyle BackColor="#246B61" />
                             <SortedDescendingCellStyle BackColor="#D4DFE1" />
                             <SortedDescendingHeaderStyle BackColor="#15524A" />
                         </asp:GridView>
                         <asp:SqlDataSource ID="SqlDataSourceuser" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT * FROM [Użytkownicy]"></asp:SqlDataSource>
                         <asp:Button ID="Button29" runat="server" OnClick="Button29_Click" Text="Odśwież" />
                         </asp:View>
                     <asp:View ID="View17" runat="server">
                         Podgląd Tabeli Zdarzenia<br />
                         <asp:GridView ID="GridView17" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_zdarzenia" DataSourceID="SqlDataSourcezdarzenia" ForeColor="#333333" GridLines="None">
                             <AlternatingRowStyle BackColor="White" />
                             <Columns>
                                 <asp:BoundField DataField="ID_zdarzenia" HeaderText="ID_zdarzenia" ReadOnly="True" SortExpression="ID_zdarzenia" />
                                 <asp:BoundField DataField="ID_meczu" HeaderText="ID_meczu" SortExpression="ID_meczu" />
                                 <asp:BoundField DataField="ID_piłkarza" HeaderText="ID_piłkarza" SortExpression="ID_piłkarza" />
                                 <asp:BoundField DataField="Czas_zdarzenia" HeaderText="Czas_zdarzenia" SortExpression="Czas_zdarzenia" />
                                 <asp:BoundField DataField="Gol" HeaderText="Gol" SortExpression="Gol" />
                                 <asp:BoundField DataField="Strata" HeaderText="Strata" SortExpression="Strata" />
                                 <asp:BoundField DataField="Czerwona_kartka" HeaderText="Czerwona_kartka" SortExpression="Czerwona_kartka" />
                                 <asp:BoundField DataField="Żółta_kartka" HeaderText="Żółta_kartka" SortExpression="Żółta_kartka" />
                             </Columns>
                             <EditRowStyle BackColor="#7C6F57" />
                             <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                             <RowStyle BackColor="#E3EAEB" />
                             <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                             <SortedAscendingCellStyle BackColor="#F8FAFA" />
                             <SortedAscendingHeaderStyle BackColor="#246B61" />
                             <SortedDescendingCellStyle BackColor="#D4DFE1" />
                             <SortedDescendingHeaderStyle BackColor="#15524A" />
                         </asp:GridView>
                         <asp:SqlDataSource ID="SqlDataSourcezdarzenia" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" SelectCommand="SELECT * FROM [ZDARZENIA]"></asp:SqlDataSource>
                         <asp:Button ID="Button30" runat="server" OnClick="Button30_Click" Text="Odśwież" />
                         </asp:View>
                 </asp:MultiView>
                 <asp:MultiView ID="MultiView4" runat="server">
                     <asp:View ID="View10" runat="server">
                         <asp:Label ID="Label18" runat="server" Text="Zarzadzanie tabelą drużyny"></asp:Label>
                         <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="ID_drużyny" DataSourceID="SqlDataSourcedrużynydetails" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                             <AlternatingRowStyle BackColor="White" />
                             <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                             <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                             <Fields>
                                 <asp:BoundField DataField="ID_drużyny" HeaderText="ID_drużyny" ReadOnly="True" SortExpression="ID_drużyny" />
                                 <asp:BoundField DataField="Nazwa_klubu" HeaderText="Nazwa_klubu" SortExpression="Nazwa_klubu" />
                                 <asp:BoundField DataField="Barwy" HeaderText="Barwy" SortExpression="Barwy" />
                                 <asp:BoundField DataField="Data_zal" HeaderText="Data_zal" SortExpression="Data_zal" />
                                 <asp:BoundField DataField="Adres" HeaderText="Adres" SortExpression="Adres" />
                                 <asp:BoundField DataField="Pozycja_ligowa" HeaderText="Pozycja_ligowa" SortExpression="Pozycja_ligowa" />
                                 <asp:BoundField DataField="Punkty" HeaderText="Punkty" SortExpression="Punkty" />
                                 <asp:BoundField DataField="ilość_zdobytych_bramek" HeaderText="ilość_zdobytych_bramek" SortExpression="ilość_zdobytych_bramek" />
                                 <asp:BoundField DataField="ilość_straconych_bramek" HeaderText="ilość_straconych_bramek" SortExpression="ilość_straconych_bramek" />
                                 <asp:BoundField DataField="Wygrane_mecze" HeaderText="Wygrane_mecze" SortExpression="Wygrane_mecze" />
                                 <asp:BoundField DataField="Przegrane_mecze" HeaderText="Przegrane_mecze" SortExpression="Przegrane_mecze" />
                                 <asp:BoundField DataField="Remisowe_mecze" HeaderText="Remisowe_mecze" SortExpression="Remisowe_mecze" />
                                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                             </Fields>
                             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                         </asp:DetailsView>
                         <asp:SqlDataSource ID="SqlDataSourcedrużynydetails" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" DeleteCommand="DELETE FROM [DRUŻYNY] WHERE [ID_drużyny] = @ID_drużyny" InsertCommand="INSERT INTO [DRUŻYNY] ([ID_drużyny], [Nazwa_klubu], [Barwy], [Data_zal], [Adres], [Pozycja_ligowa], [Punkty], [ilość_zdobytych_bramek], [ilość_straconych_bramek], [Wygrane_mecze], [Przegrane_mecze], [Remisowe_mecze]) VALUES (@ID_drużyny, @Nazwa_klubu, @Barwy, @Data_zal, @Adres, @Pozycja_ligowa, @Punkty, @ilość_zdobytych_bramek, @ilość_straconych_bramek, @Wygrane_mecze, @Przegrane_mecze, @Remisowe_mecze)" SelectCommand="SELECT * FROM [DRUŻYNY]" UpdateCommand="UPDATE [DRUŻYNY] SET [Nazwa_klubu] = @Nazwa_klubu, [Barwy] = @Barwy, [Data_zal] = @Data_zal, [Adres] = @Adres, [Pozycja_ligowa] = @Pozycja_ligowa, [Punkty] = @Punkty, [ilość_zdobytych_bramek] = @ilość_zdobytych_bramek, [ilość_straconych_bramek] = @ilość_straconych_bramek, [Wygrane_mecze] = @Wygrane_mecze, [Przegrane_mecze] = @Przegrane_mecze, [Remisowe_mecze] = @Remisowe_mecze WHERE [ID_drużyny] = @ID_drużyny">
                             <DeleteParameters>
                                 <asp:Parameter Name="ID_drużyny" Type="Int32" />
                             </DeleteParameters>
                             <InsertParameters>
                                 <asp:Parameter Name="ID_drużyny" Type="Int32" />
                                 <asp:Parameter Name="Nazwa_klubu" Type="String" />
                                 <asp:Parameter Name="Barwy" Type="String" />
                                 <asp:Parameter Name="Data_zal" Type="DateTime" />
                                 <asp:Parameter Name="Adres" Type="String" />
                                 <asp:Parameter Name="Pozycja_ligowa" Type="Int32" />
                                 <asp:Parameter Name="Punkty" Type="Int32" />
                                 <asp:Parameter Name="ilość_zdobytych_bramek" Type="Int32" />
                                 <asp:Parameter Name="ilość_straconych_bramek" Type="Int32" />
                                 <asp:Parameter Name="Wygrane_mecze" Type="Int32" />
                                 <asp:Parameter Name="Przegrane_mecze" Type="Int32" />
                                 <asp:Parameter Name="Remisowe_mecze" Type="Int32" />
                             </InsertParameters>
                             <UpdateParameters>
                                 <asp:Parameter Name="Nazwa_klubu" Type="String" />
                                 <asp:Parameter Name="Barwy" Type="String" />
                                 <asp:Parameter Name="Data_zal" Type="DateTime" />
                                 <asp:Parameter Name="Adres" Type="String" />
                                 <asp:Parameter Name="Pozycja_ligowa" Type="Int32" />
                                 <asp:Parameter Name="Punkty" Type="Int32" />
                                 <asp:Parameter Name="ilość_zdobytych_bramek" Type="Int32" />
                                 <asp:Parameter Name="ilość_straconych_bramek" Type="Int32" />
                                 <asp:Parameter Name="Wygrane_mecze" Type="Int32" />
                                 <asp:Parameter Name="Przegrane_mecze" Type="Int32" />
                                 <asp:Parameter Name="Remisowe_mecze" Type="Int32" />
                                 <asp:Parameter Name="ID_drużyny" Type="Int32" />
                             </UpdateParameters>
                         </asp:SqlDataSource>
                         </asp:View>
                     <asp:View ID="View18" runat="server">
                         Zarzadzanie Tabelą Mecze<asp:DetailsView ID="DetailsView2" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="ID_meczu" DataSourceID="SqlDataSourcemeczeedit" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                             <AlternatingRowStyle BackColor="White" />
                             <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                             <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                             <Fields>
                                 <asp:BoundField DataField="ID_meczu" HeaderText="ID_meczu" ReadOnly="True" SortExpression="ID_meczu" />
                                 <asp:BoundField DataField="Kolejka" HeaderText="Kolejka" SortExpression="Kolejka" />
                                 <asp:BoundField DataField="Data_meczu" HeaderText="Data_meczu" SortExpression="Data_meczu" />
                                 <asp:BoundField DataField="Godzina" HeaderText="Godzina" SortExpression="Godzina" />
                                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                             </Fields>
                             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                         </asp:DetailsView>
                         <asp:SqlDataSource ID="SqlDataSourcemeczeedit" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" DeleteCommand="DELETE FROM [MECZE] WHERE [ID_meczu] = @ID_meczu" InsertCommand="INSERT INTO [MECZE] ([ID_meczu], [Kolejka], [Data_meczu], [Godzina]) VALUES (@ID_meczu, @Kolejka, @Data_meczu, @Godzina)" SelectCommand="SELECT * FROM [MECZE]" UpdateCommand="UPDATE [MECZE] SET [Kolejka] = @Kolejka, [Data_meczu] = @Data_meczu, [Godzina] = @Godzina WHERE [ID_meczu] = @ID_meczu">
                             <DeleteParameters>
                                 <asp:Parameter Name="ID_meczu" Type="Int32" />
                             </DeleteParameters>
                             <InsertParameters>
                                 <asp:Parameter Name="ID_meczu" Type="Int32" />
                                 <asp:Parameter Name="Kolejka" Type="Int32" />
                                 <asp:Parameter Name="Data_meczu" Type="DateTime" />
                                 <asp:Parameter Name="Godzina" Type="DateTime" />
                             </InsertParameters>
                             <UpdateParameters>
                                 <asp:Parameter Name="Kolejka" Type="Int32" />
                                 <asp:Parameter Name="Data_meczu" Type="DateTime" />
                                 <asp:Parameter Name="Godzina" Type="DateTime" />
                                 <asp:Parameter Name="ID_meczu" Type="Int32" />
                             </UpdateParameters>
                         </asp:SqlDataSource>
                         </asp:View>
                     <asp:View ID="View19" runat="server">
                         Zarządzanie tabelą Piłkarze<asp:DetailsView ID="DetailsView3" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="ID_piłkarza" DataSourceID="SqlDataSourcepiłkarzemenage" ForeColor="#333333" GridLines="None" Height="50px" Width="164px">
                             <AlternatingRowStyle BackColor="White" />
                             <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                             <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                             <Fields>
                                 <asp:BoundField DataField="ID_piłkarza" HeaderText="ID_piłkarza" ReadOnly="True" SortExpression="ID_piłkarza" />
                                 <asp:BoundField DataField="ID_drużyny" HeaderText="ID_drużyny" SortExpression="ID_drużyny" />
                                 <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                 <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                                 <asp:BoundField DataField="Data_ur" HeaderText="Data_ur" SortExpression="Data_ur" />
                                 <asp:BoundField DataField="Pozycja" HeaderText="Pozycja" SortExpression="Pozycja" />
                                 <asp:BoundField DataField="Waga" HeaderText="Waga" SortExpression="Waga" />
                                 <asp:BoundField DataField="Wzrost" HeaderText="Wzrost" SortExpression="Wzrost" />
                                 <asp:BoundField DataField="Szybkosc" HeaderText="Szybkosc" SortExpression="Szybkosc" />
                                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                             </Fields>
                             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                         </asp:DetailsView>
                         <asp:SqlDataSource ID="SqlDataSourcepiłkarzemenage" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" DeleteCommand="DELETE FROM [PIŁKARZE] WHERE [ID_piłkarza] = @ID_piłkarza" InsertCommand="INSERT INTO [PIŁKARZE] ([ID_piłkarza], [ID_drużyny], [Nazwisko], [Imie], [Data_ur], [Pozycja], [Waga], [Wzrost], [Szybkosc]) VALUES (@ID_piłkarza, @ID_drużyny, @Nazwisko, @Imie, @Data_ur, @Pozycja, @Waga, @Wzrost, @Szybkosc)" SelectCommand="SELECT * FROM [PIŁKARZE]" UpdateCommand="UPDATE [PIŁKARZE] SET [ID_drużyny] = @ID_drużyny, [Nazwisko] = @Nazwisko, [Imie] = @Imie, [Data_ur] = @Data_ur, [Pozycja] = @Pozycja, [Waga] = @Waga, [Wzrost] = @Wzrost, [Szybkosc] = @Szybkosc WHERE [ID_piłkarza] = @ID_piłkarza">
                             <DeleteParameters>
                                 <asp:Parameter Name="ID_piłkarza" Type="Int32" />
                             </DeleteParameters>
                             <InsertParameters>
                                 <asp:Parameter Name="ID_piłkarza" Type="Int32" />
                                 <asp:Parameter Name="ID_drużyny" Type="Int32" />
                                 <asp:Parameter Name="Nazwisko" Type="String" />
                                 <asp:Parameter Name="Imie" Type="String" />
                                 <asp:Parameter Name="Data_ur" Type="DateTime" />
                                 <asp:Parameter Name="Pozycja" Type="String" />
                                 <asp:Parameter Name="Waga" Type="Int32" />
                                 <asp:Parameter Name="Wzrost" Type="Int32" />
                                 <asp:Parameter Name="Szybkosc" Type="Int32" />
                             </InsertParameters>
                             <UpdateParameters>
                                 <asp:Parameter Name="ID_drużyny" Type="Int32" />
                                 <asp:Parameter Name="Nazwisko" Type="String" />
                                 <asp:Parameter Name="Imie" Type="String" />
                                 <asp:Parameter Name="Data_ur" Type="DateTime" />
                                 <asp:Parameter Name="Pozycja" Type="String" />
                                 <asp:Parameter Name="Waga" Type="Int32" />
                                 <asp:Parameter Name="Wzrost" Type="Int32" />
                                 <asp:Parameter Name="Szybkosc" Type="Int32" />
                                 <asp:Parameter Name="ID_piłkarza" Type="Int32" />
                             </UpdateParameters>
                         </asp:SqlDataSource>
                         </asp:View>
                     <asp:View ID="View20" runat="server">
                         Zarządzanie tabelą Sędziowie<asp:DetailsView ID="DetailsView4" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="ID_sedziego" DataSourceID="SqlDataSourcesedziowemenage" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                             <AlternatingRowStyle BackColor="White" />
                             <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                             <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                             <Fields>
                                 <asp:BoundField DataField="ID_sedziego" HeaderText="ID_sedziego" ReadOnly="True" SortExpression="ID_sedziego" />
                                 <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                 <asp:BoundField DataField="Imie" HeaderText="Imie" SortExpression="Imie" />
                                 <asp:BoundField DataField="Data_ur" HeaderText="Data_ur" SortExpression="Data_ur" />
                                 <asp:BoundField DataField="Ilość_przep_meczy" HeaderText="Ilość_przep_meczy" SortExpression="Ilość_przep_meczy" />
                                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                             </Fields>
                             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                         </asp:DetailsView>
                         <asp:SqlDataSource ID="SqlDataSourcesedziowemenage" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" DeleteCommand="DELETE FROM [SĘDZIOWIE] WHERE [ID_sedziego] = @ID_sedziego" InsertCommand="INSERT INTO [SĘDZIOWIE] ([ID_sedziego], [Nazwisko], [Imie], [Data_ur], [Ilość_przep_meczy]) VALUES (@ID_sedziego, @Nazwisko, @Imie, @Data_ur, @Ilość_przep_meczy)" SelectCommand="SELECT * FROM [SĘDZIOWIE]" UpdateCommand="UPDATE [SĘDZIOWIE] SET [Nazwisko] = @Nazwisko, [Imie] = @Imie, [Data_ur] = @Data_ur, [Ilość_przep_meczy] = @Ilość_przep_meczy WHERE [ID_sedziego] = @ID_sedziego">
                             <DeleteParameters>
                                 <asp:Parameter Name="ID_sedziego" Type="Int32" />
                             </DeleteParameters>
                             <InsertParameters>
                                 <asp:Parameter Name="ID_sedziego" Type="Int32" />
                                 <asp:Parameter Name="Nazwisko" Type="String" />
                                 <asp:Parameter Name="Imie" Type="String" />
                                 <asp:Parameter Name="Data_ur" Type="DateTime" />
                                 <asp:Parameter Name="Ilość_przep_meczy" Type="Int32" />
                             </InsertParameters>
                             <UpdateParameters>
                                 <asp:Parameter Name="Nazwisko" Type="String" />
                                 <asp:Parameter Name="Imie" Type="String" />
                                 <asp:Parameter Name="Data_ur" Type="DateTime" />
                                 <asp:Parameter Name="Ilość_przep_meczy" Type="Int32" />
                                 <asp:Parameter Name="ID_sedziego" Type="Int32" />
                             </UpdateParameters>
                         </asp:SqlDataSource>
                         </asp:View>
                     <asp:View ID="View21" runat="server">
                         Zarządzanie tabelą sedzia meczu<asp:DetailsView ID="DetailsView5" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="ID_meczu,ID_sedziego" DataSourceID="SqlDataSourcesedziameczumenage" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                             <AlternatingRowStyle BackColor="White" />
                             <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                             <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                             <Fields>
                                 <asp:BoundField DataField="ID_meczu" HeaderText="ID_meczu" ReadOnly="True" SortExpression="ID_meczu" />
                                 <asp:BoundField DataField="ID_sedziego" HeaderText="ID_sedziego" ReadOnly="True" SortExpression="ID_sedziego" />
                                 <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                             </Fields>
                             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                         </asp:DetailsView>
                         <asp:SqlDataSource ID="SqlDataSourcesedziameczumenage" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" DeleteCommand="DELETE FROM [Sędzia_meczu] WHERE [ID_meczu] = @ID_meczu AND [ID_sedziego] = @ID_sedziego" InsertCommand="INSERT INTO [Sędzia_meczu] ([ID_meczu], [ID_sedziego], [Status]) VALUES (@ID_meczu, @ID_sedziego, @Status)" SelectCommand="SELECT * FROM [Sędzia_meczu]" UpdateCommand="UPDATE [Sędzia_meczu] SET [Status] = @Status WHERE [ID_meczu] = @ID_meczu AND [ID_sedziego] = @ID_sedziego">
                             <DeleteParameters>
                                 <asp:Parameter Name="ID_meczu" Type="Int32" />
                                 <asp:Parameter Name="ID_sedziego" Type="Int32" />
                             </DeleteParameters>
                             <InsertParameters>
                                 <asp:Parameter Name="ID_meczu" Type="Int32" />
                                 <asp:Parameter Name="ID_sedziego" Type="Int32" />
                                 <asp:Parameter Name="Status" Type="String" />
                             </InsertParameters>
                             <UpdateParameters>
                                 <asp:Parameter Name="Status" Type="String" />
                                 <asp:Parameter Name="ID_meczu" Type="Int32" />
                                 <asp:Parameter Name="ID_sedziego" Type="Int32" />
                             </UpdateParameters>
                         </asp:SqlDataSource>
                         </asp:View>
                     <asp:View ID="View22" runat="server">
                         zarządzanie tabelą trenerzy<asp:DetailsView ID="DetailsView6" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="ID_trenera" DataSourceID="SqlDataSourcetrenerzymenage" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                             <AlternatingRowStyle BackColor="White" />
                             <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                             <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                             <Fields>
                                 <asp:BoundField DataField="ID_trenera" HeaderText="ID_trenera" ReadOnly="True" SortExpression="ID_trenera" />
                                 <asp:BoundField DataField="ID_drużyny" HeaderText="ID_drużyny" SortExpression="ID_drużyny" />
                                 <asp:BoundField DataField="Nazwisko" HeaderText="Nazwisko" SortExpression="Nazwisko" />
                                 <asp:BoundField DataField="Imię" HeaderText="Imię" SortExpression="Imię" />
                                 <asp:BoundField DataField="Data_ur" HeaderText="Data_ur" SortExpression="Data_ur" />
                                 <asp:BoundField DataField="Stopien_trenera" HeaderText="Stopien_trenera" SortExpression="Stopien_trenera" />
                                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                             </Fields>
                             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                         </asp:DetailsView>
                         <asp:SqlDataSource ID="SqlDataSourcetrenerzymenage" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" DeleteCommand="DELETE FROM [TRENERZY] WHERE [ID_trenera] = @ID_trenera" InsertCommand="INSERT INTO [TRENERZY] ([ID_trenera], [ID_drużyny], [Nazwisko], [Imię], [Data_ur], [Stopien_trenera]) VALUES (@ID_trenera, @ID_drużyny, @Nazwisko, @Imię, @Data_ur, @Stopien_trenera)" SelectCommand="SELECT * FROM [TRENERZY]" UpdateCommand="UPDATE [TRENERZY] SET [ID_drużyny] = @ID_drużyny, [Nazwisko] = @Nazwisko, [Imię] = @Imię, [Data_ur] = @Data_ur, [Stopien_trenera] = @Stopien_trenera WHERE [ID_trenera] = @ID_trenera">
                             <DeleteParameters>
                                 <asp:Parameter Name="ID_trenera" Type="Int32" />
                             </DeleteParameters>
                             <InsertParameters>
                                 <asp:Parameter Name="ID_trenera" Type="Int32" />
                                 <asp:Parameter Name="ID_drużyny" Type="Int32" />
                                 <asp:Parameter Name="Nazwisko" Type="String" />
                                 <asp:Parameter Name="Imię" Type="String" />
                                 <asp:Parameter Name="Data_ur" Type="DateTime" />
                                 <asp:Parameter Name="Stopien_trenera" Type="String" />
                             </InsertParameters>
                             <UpdateParameters>
                                 <asp:Parameter Name="ID_drużyny" Type="Int32" />
                                 <asp:Parameter Name="Nazwisko" Type="String" />
                                 <asp:Parameter Name="Imię" Type="String" />
                                 <asp:Parameter Name="Data_ur" Type="DateTime" />
                                 <asp:Parameter Name="Stopien_trenera" Type="String" />
                                 <asp:Parameter Name="ID_trenera" Type="Int32" />
                             </UpdateParameters>
                         </asp:SqlDataSource>
                         </asp:View>
                     <asp:View ID="View23" runat="server">
                         Zarządzanie tabelą użytkownicy<asp:DetailsView ID="DetailsView7" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="Nazwa_użytkownika" DataSourceID="SqlDataSourceusermanage" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                             <AlternatingRowStyle BackColor="White" />
                             <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                             <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                             <Fields>
                                 <asp:BoundField DataField="Nazwa_użytkownika" HeaderText="Nazwa_użytkownika" ReadOnly="True" SortExpression="Nazwa_użytkownika" />
                                 <asp:BoundField DataField="Haslo" HeaderText="Haslo" SortExpression="Haslo" />
                                 <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                             </Fields>
                             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                         </asp:DetailsView>
                         <asp:SqlDataSource ID="SqlDataSourceusermanage" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" DeleteCommand="DELETE FROM [Użytkownicy] WHERE [Nazwa_użytkownika] = @Nazwa_użytkownika" InsertCommand="INSERT INTO [Użytkownicy] ([Nazwa_użytkownika], [Haslo], [Status]) VALUES (@Nazwa_użytkownika, @Haslo, @Status)" SelectCommand="SELECT * FROM [Użytkownicy]" UpdateCommand="UPDATE [Użytkownicy] SET [Haslo] = @Haslo, [Status] = @Status WHERE [Nazwa_użytkownika] = @Nazwa_użytkownika">
                             <DeleteParameters>
                                 <asp:Parameter Name="Nazwa_użytkownika" Type="String" />
                             </DeleteParameters>
                             <InsertParameters>
                                 <asp:Parameter Name="Nazwa_użytkownika" Type="String" />
                                 <asp:Parameter Name="Haslo" Type="String" />
                                 <asp:Parameter Name="Status" Type="String" />
                             </InsertParameters>
                             <UpdateParameters>
                                 <asp:Parameter Name="Haslo" Type="String" />
                                 <asp:Parameter Name="Status" Type="String" />
                                 <asp:Parameter Name="Nazwa_użytkownika" Type="String" />
                             </UpdateParameters>
                         </asp:SqlDataSource>
                         </asp:View>
                     <asp:View ID="View24" runat="server">
                         Zarządzanie tabelą zdarzenia<br />
                         <asp:DetailsView ID="DetailsView8" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="ID_zdarzenia" DataSourceID="SqlDataSourcezdarzeniamenage" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                             <AlternatingRowStyle BackColor="White" />
                             <CommandRowStyle BackColor="#FFFFC0" Font-Bold="True" />
                             <FieldHeaderStyle BackColor="#FFFF99" Font-Bold="True" />
                             <Fields>
                                 <asp:BoundField DataField="ID_zdarzenia" HeaderText="ID_zdarzenia" ReadOnly="True" SortExpression="ID_zdarzenia" />
                                 <asp:BoundField DataField="ID_meczu" HeaderText="ID_meczu" SortExpression="ID_meczu" />
                                 <asp:BoundField DataField="ID_piłkarza" HeaderText="ID_piłkarza" SortExpression="ID_piłkarza" />
                                 <asp:BoundField DataField="Czas_zdarzenia" HeaderText="Czas_zdarzenia" SortExpression="Czas_zdarzenia" />
                                 <asp:BoundField DataField="Gol" HeaderText="Gol" SortExpression="Gol" />
                                 <asp:BoundField DataField="Strata" HeaderText="Strata" SortExpression="Strata" />
                                 <asp:BoundField DataField="Czerwona_kartka" HeaderText="Czerwona_kartka" SortExpression="Czerwona_kartka" />
                                 <asp:BoundField DataField="Żółta_kartka" HeaderText="Żółta_kartka" SortExpression="Żółta_kartka" />
                                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                             </Fields>
                             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                         </asp:DetailsView>
                         <asp:SqlDataSource ID="SqlDataSourcezdarzeniamenage" runat="server" ConnectionString="<%$ ConnectionStrings:Ekstraklasa_VER_1_1ConnectionString %>" DeleteCommand="DELETE FROM [ZDARZENIA] WHERE [ID_zdarzenia] = @ID_zdarzenia" InsertCommand="INSERT INTO [ZDARZENIA] ([ID_zdarzenia], [ID_meczu], [ID_piłkarza], [Czas_zdarzenia], [Gol], [Strata], [Czerwona_kartka], [Żółta_kartka]) VALUES (@ID_zdarzenia, @ID_meczu, @ID_piłkarza, @Czas_zdarzenia, @Gol, @Strata, @Czerwona_kartka, @Żółta_kartka)" SelectCommand="SELECT * FROM [ZDARZENIA]" UpdateCommand="UPDATE [ZDARZENIA] SET [ID_meczu] = @ID_meczu, [ID_piłkarza] = @ID_piłkarza, [Czas_zdarzenia] = @Czas_zdarzenia, [Gol] = @Gol, [Strata] = @Strata, [Czerwona_kartka] = @Czerwona_kartka, [Żółta_kartka] = @Żółta_kartka WHERE [ID_zdarzenia] = @ID_zdarzenia">
                             <DeleteParameters>
                                 <asp:Parameter Name="ID_zdarzenia" Type="Int32" />
                             </DeleteParameters>
                             <InsertParameters>
                                 <asp:Parameter Name="ID_zdarzenia" Type="Int32" />
                                 <asp:Parameter Name="ID_meczu" Type="Int32" />
                                 <asp:Parameter Name="ID_piłkarza" Type="Int32" />
                                 <asp:Parameter Name="Czas_zdarzenia" Type="DateTime" />
                                 <asp:Parameter Name="Gol" Type="Int32" />
                                 <asp:Parameter Name="Strata" Type="Int32" />
                                 <asp:Parameter Name="Czerwona_kartka" Type="Int32" />
                                 <asp:Parameter Name="Żółta_kartka" Type="Int32" />
                             </InsertParameters>
                             <UpdateParameters>
                                 <asp:Parameter Name="ID_meczu" Type="Int32" />
                                 <asp:Parameter Name="ID_piłkarza" Type="Int32" />
                                 <asp:Parameter Name="Czas_zdarzenia" Type="DateTime" />
                                 <asp:Parameter Name="Gol" Type="Int32" />
                                 <asp:Parameter Name="Strata" Type="Int32" />
                                 <asp:Parameter Name="Czerwona_kartka" Type="Int32" />
                                 <asp:Parameter Name="Żółta_kartka" Type="Int32" />
                                 <asp:Parameter Name="ID_zdarzenia" Type="Int32" />
                             </UpdateParameters>
                         </asp:SqlDataSource>
                         </asp:View>
                     </asp:MultiView>
                 </asp:View>
        </asp:MultiView>
    </form>
</body>
</html>
