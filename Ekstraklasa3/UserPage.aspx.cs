﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ekstraklasa3
{
    public partial class UserPage : System.Web.UI.Page
    {
        String RecivedValue = " ";
        String UserRecived = " ";
        protected void Page_Load(object sender, EventArgs e)
        {
            RecivedValue =   Request.QueryString["status"];
            UserRecived = Request.QueryString["user"];
            Label5.Text = "Zalogowano użytkownika " + UserRecived;
            if (RecivedValue == "kibic")
            {
                LinkButton4.Visible = false;
                LinkButton5.Visible = false;
            }
            if (RecivedValue == "admin")
            {
                LinkButton4.Visible = true;
                LinkButton5.Visible = true;
            }
            if (RecivedValue == "pzpn")
            {
                LinkButton4.Visible = true;
                LinkButton5.Visible = false;
            }
        }
        public void filtracjaWyniki()
        {
            String drużyna = DropDownListspotkania1.Text;
            String Kolejka = DropDownListspotkania2.Text;
            String Query = "  Where g1.Gospodarz != ' ' ";
            if (CheckBoxspotkania1.Checked == true)
            {
                Query = Query + " AND (g1.Gospodarz like '" + drużyna + "' OR g2.Gość like '" + drużyna + "') ";
            }
            if (CheckBoxspotkania2.Checked)
            {
                Query = Query + " AND g1.Kolejka like '" + Kolejka + "'";
            }
            SqlDataSourcespotkania.SelectCommand = "SELECT g1.Gospodarz as Gospodarz, g1.[Wynik gospodarz] , g2.[Wynik gość], g2.Gość as Gość , g1.Data_Spotkania as 'Data Spotkania', g1.Kolejka" +
            " FROM gospodarz_view g1" +
            " LEFT JOIN gość_view g2" +
                " ON g1.mecze = g2.mecz  " +
                Query +
                    "  order by g1.kolejka";


        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void wyszukajSpotkania_Click(object sender, EventArgs e)
        {
            filtracjaWyniki();
        }

        

        protected void LinkButton6_Click(object sender, EventArgs e)
        {
            MultiView2.ActiveViewIndex = 0;
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
        }

        protected void LinkButton7_Click(object sender, EventArgs e)
        {
            MultiView2.ActiveViewIndex = 1;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (Label7.Visible == false && GridView4.Visible == false)
            {
                Label7.Visible = true;
                GridView4.Visible = true;
            }
            else
            {
                Label7.Visible = false;
                GridView4.Visible = false;
            }

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            if (Label8.Visible == false && GridView5.Visible == false)
            {
                Label8.Visible = true;
                GridView5.Visible = true;
            }
            else
            {
                Label8.Visible = false;
                GridView5.Visible = false;
            }
        }

        protected void LinkButton8_Click(object sender, EventArgs e)
        {
            MultiView2.ActiveViewIndex = 2;
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
        }

        public void FiltracjaHarmonogram()
        {
            String datefirst = "";
            String FiltersDatefirst = "";
            String FiltersDateSecond = "";
            if (CheckBoxHarmo.Checked == true)
            {
                DateTime data1 = Calendar2.SelectedDate;
                DateTime data2 = Calendar1.SelectedDate;
                FiltersDatefirst = Convert.ToString(data1);
                FiltersDateSecond = Convert.ToString(data2);
                datefirst = " AND (g1.Data_Spotkania > '" + FiltersDatefirst + "' AND g1.Data_Spotkania < '" + FiltersDateSecond + "' ) ";
             
            }
            SqlDataSourceHarmonogram.SelectCommand = "SELECT g1.Gospodarz as Gospodarz, g2.Gość as Gość , g1.Data_Spotkania as 'Data Spotkania', g1.Kolejka " +
                 "FROM gospodarz_view g1 " +
                 "LEFT JOIN gość_view g2 " +
                 "ON g1.mecze = g2.mecz "
                 + "WHERE  g1.Gospodarz <>'' " + datefirst
                 + " order by g1.kolejka";


        }
        public void Szczegolypracysedziego()
        {
            String arbiternum = DropDownList1.Text;

            SqlDataSourceSzczegółypracy.SelectCommand = "Select z.ID_zdarzenia as'Numer Zdarzenia', z.ID_meczu as 'Numer Spotkania'," +
                " z.ID_piłkarza as 'Numer Piłkarza', z.Czas_zdarzenia, z.Gol , z.Strata," +
                " z.Czerwona_kartka as 'Czerwona kartka', z.Żółta_kartka as 'Żółta kartka'" +
                " from ZDARZENIA z ,mecze m , Sędzia_meczu sm" +
                " where sm.ID_sedziego = " + "'" + arbiternum + "' and sm.ID_meczu = z.ID_meczu" +
                " group by z.ID_meczu,z.ID_zdarzenia,z.ID_piłkarza,z.Czas_zdarzenia,z.Gol,z.Strata,z.Czerwona_kartka,z.Żółta_kartka;";


            if (Label14.Visible == false && GridView9.Visible == false)
            {
                Label14.Visible = true;
                GridView9.Visible = true;
            }
            else
            {
                Label14.Visible = false;
                GridView9.Visible = false;
            }
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            FiltracjaHarmonogram();
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Szczegolypracysedziego();
        }

        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 3;
        }

        protected void LinkButton5_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 4;
        }

        protected void LinkButton9_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 0;
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            GridView10.DataBind();
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 0;
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            MultiView4.ActiveViewIndex = 0;
        }

        protected void Button24_Click(object sender, EventArgs e)
        {
            GridView11.DataBind();
        }

        protected void Button25_Click(object sender, EventArgs e)
        {
            GridView12.DataBind();
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 1;
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 2;
        }

        protected void Button26_Click(object sender, EventArgs e)
        {
            GridView13.DataBind();
        }

        protected void Button14_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 3;
        }

        protected void Button27_Click(object sender, EventArgs e)
        {
            GridView14.DataBind();
        }

        protected void Button16_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 4;
        }

        protected void Button28_Click(object sender, EventArgs e)
        {
            GridView15.DataBind();
        }

        protected void Button18_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 5;
        }

        protected void Button29_Click(object sender, EventArgs e)
        {
            GridView16.DataBind();
        }

        protected void Button20_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 6;
        }

        protected void Button30_Click(object sender, EventArgs e)
        {
            GridView17.DataBind();
        }

        protected void Button22_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 7;
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            MultiView4.ActiveViewIndex = 1;
        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            MultiView4.ActiveViewIndex = 2;
        }

        protected void Button15_Click(object sender, EventArgs e)
        {
            MultiView4.ActiveViewIndex = 3;
        }

        protected void Button17_Click(object sender, EventArgs e)
        {
            MultiView4.ActiveViewIndex = 4;
        }

        protected void Button19_Click(object sender, EventArgs e)
        {
            MultiView4.ActiveViewIndex = 5;
        }

        protected void Button21_Click(object sender, EventArgs e)
        {
            MultiView4.ActiveViewIndex = 6;
        }

        protected void Button23_Click(object sender, EventArgs e)
        {
            MultiView4.ActiveViewIndex = 7;
        }

       

        

    }
}